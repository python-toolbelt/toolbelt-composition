# Changelog

Tracking changes in `toolbelt-composition` between versions. For a complete view of all the releases, visit GitLab:

https://gitlab.com/python-toolbelt/toolbelt-composition/-/releases

## next release
