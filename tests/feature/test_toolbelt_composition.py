from toolbelt.composition import toolbelt_composition

def test_toolbelt_composition():
  assert toolbelt_composition() == 'toolbelt_composition'
